import React from 'react'

class CustomDashboard extends React.Component {
    render() {
        return (
            <div className="mgs-container">
                <div className="mgs-row">
                    <div className="box">
                        <h3>Monitoring Contol Panel</h3>
                        <p>
                            This is a <a href="http://crudl.io/" target="_blank">CP</a> example with UI, based on a JSON-like models.
                        </p>
                        <p>
                            Prototype is based on crudl library (MIT-licensed) which is a a backend agnostic React/Redux application in order to rapidly build a beautiful administration interface based on your API (REST, GraphQL, or dynamic JSON model*).
                        </p>
                    </div>
                    <div className="box">
                        <h3>Links</h3>
                        <ul>
                            <li><a href="http://crudl.io" target="_blank">crudl.io</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default CustomDashboard

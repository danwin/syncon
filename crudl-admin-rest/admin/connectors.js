import { defaults } from '@crudlio/crudl-connectors-drf'
import { createLSConnector } from './crudl-connectors-local'
import { numberedPagination } from '@crudlio/crudl-connectors-drf/lib/middleware'
import { transformData } from '@crudlio/crudl-connectors-base/lib/middleware'

defaults.baseURL = '/rest-api/'

export const list = createLSConnector(':collection/', {isList: true})
    .use(numberedPagination())

export const detail = createLSConnector(':collection/:id/')

// Resolves to { options: [{value, label}, {value, label}, ... ] }
export const options = (collection, valueKey, labelKey) => list(collection)
    .use(next => ({
        read: req => next.read(req.filter('limit', 1000000)).then(res => Object.assign(res, {
            data: {
                options: res.data.map(item => ({
                    value: item[valueKey],
                    label: item[labelKey],
                }))
            },
        })),
    }))

export const login = createLSConnector('login/')
    .use(transformData('create',
        data => ({
            requestHeaders: { "Authorization": `Token ${data.token}` },
            info: data,
        })
    ))

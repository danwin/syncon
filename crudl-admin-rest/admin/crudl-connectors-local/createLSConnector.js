import { createFrontendConnector } from '@crudlio/crudl-connectors-base'
import createBackendConnector from './createBackendConnector'
import { crudToHttp, url } from '@crudlio/crudl-connectors-base/lib/middleware'

import { buildQuery, crudlErrors } from '@crudlio/crudl-connectors-drf/lib/middleware'
import defaults from '@crudlio/crudl-connectors-drf/lib/defaults'

export default function createLSConnector(urlPath, opts) {
    if (typeof urlPath !== 'string') {
        throw new Error(`URL must be a string, found ${urlPath}.`)
    }

    const options = Object.assign({}, defaults, opts) // , {basePath: 'smubApp'}
    const lsOptions = Object.assign({}, options, { basePath: options.baseURL, isList: !!options.isList })

    const ls = createFrontendConnector(createBackendConnector(lsOptions))
    .use(crudToHttp(options.crudToHttp))
    .use(buildQuery(options.query))
    .use(url(urlPath))
    .use(crudlErrors)

    return ls
}

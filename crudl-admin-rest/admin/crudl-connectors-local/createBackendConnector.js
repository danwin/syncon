import store from 'store';
import objectPath from 'object-path';
import _ from 'underscore';
import uuid4 from 'uuid4';

/**
* This is a general backend connector that uses store.js to execute API calls.
* It requires a request object with the following attributes:
*
* url:        a string value: required
* data:       an object or an array; optional
* headers:    an object of the form { <HeaderName>: <Value> }; optional
*/
export default function createBackendConnector(lsConfig = {}) {
    var local = {}, 
        basePath = lsConfig.baseURL, 
        isList = !!lsConfig.isList,
        keyName = isList ? lsConfig.keyName || 'id' : null;
    
    function url2path(url) {
        return url
            .split('?')
            .shift()
            .replace(/\/$/, '')
            .replace('/','.');
    }

    function url2query(url) {
        return url.split('?').pop() || null;
    }

    function list2obj(list, key){
        var buff = {}
        key = key || 'id'
        _.each(list, (item)=> {
            var keyValue = item[key], pair = {}
            pair[keyValue] = item
            buff = Object.assign({}, buff, pair)
        })
        return buff
    }

    function obj2list(obj) {
        var buff = []
        _.each(obj, (item, keyValue)=>{
            buff.push(item)
        })
        return buff
    }

    function wrapResponse(data, withPages) {
        if (withPages) {
            data = Object.assign({}, {results: data}, {
                next: null, 
                previous: null,
                total: data.length,
                count: data.length
            })
            // {"total":3,"count":3,"next":null,"previous":null,"results":[{"id":3,"username":"admin","first_name":"Jane","last_name":"Citizen","email":"","is_staff":true,"is_active":true,"date_joined":"2016-06-28T08:45:14.078000"},{"id":1,"username":"demo","first_name":"John","last_name":"Doe","email":"","is_staff":true,"is_active":true,"date_joined":"2016-06-28T08:45:14.078000"},{"id":2,"username":"editor","first_name":"Joe","last_name":"Bloggs","email":"","is_staff":true,"is_active":true,"date_joined":"2016-06-28T08:45:14.078000"}]}
        }
        return {
            data: data,
            status: data.status || 200,
            statusText: data.statusText || 'Ok',
            headers: data.headers || {},
            config: {},
            request: {}
        }
    }

    local.create = function(req) {
        // { url: '/users/', httpMethod: 'post', data: { firstName: 'Jane' }}
        console.log('->',basePath, req);
        var db = store.get(basePath) || {}, 
            path = url2path(req.url),
            oldRec = _.extend({}, objectPath.get(db, path) || {}),
            newRec = _.extend({}, oldRec),
            itemData = _.extend({}, req.data),
            keyValue = itemData[keyName] || uuid4();
        
        function pair2obj(key, value){
            var obj = {}
            obj[key] = value
            return obj
        }
        
        // Ensure that key exists:
        itemData[keyName] = keyValue

        if (isList) {
            // Key-value storage:
            itemData = _.extend(oldRec[keyValue] || {}, itemData)
            newRec = _.extend(oldRec, pair2obj(keyValue, itemData))
        } else {
            // Single "atomic" value
            itemData = _.extend(oldRec, itemData)
            newRec = itemData
        }
        objectPath.set(db, path, newRec)
        store.set(basePath, db)
        console.warn('CREATE', req.url, wrapResponse(req.data));
        return Promise.resolve(wrapResponse(req.data))
    }
    local.update = local.create;
    local.read = function(req) {
        // { url: '/users/', httpMethod: 'post', data: { firstName: 'Jane' }}
        var db = store.get(basePath) || {}, 
            path = url2path(req.url),
            query = url2query(req.url), 
            value = objectPath.get(db, path) || {};
        if (isList) value = obj2list(value)
        console.warn('q->', path, db);
        console.warn('READ', req.url, wrapResponse(value, isList));
        return Promise.resolve(wrapResponse(value, isList))
    }
    local.delete = function(req) {
        // { url: '/users/', httpMethod: 'post', data: { firstName: 'Jane' }}
        var db = store.get(basePath), path = url2path(req.url), value = objectPath.get(db, path);
        if (typeof value !== 'undefined') {
            store.remove(path);
            console.warn('DELETE', req.url, wrapResponse(req.data));
            return Promise.resolve(wrapResponse({}))
        }
        return Promise.reject(wrapResponse({status: 404, statusText: 'Not found: ' + path}))
    }

    return {
        create: local.create,
        read: local.read,
        update: local.update,
        delete: local.delete,
    }
}

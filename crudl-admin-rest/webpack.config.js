var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: ['./admin/index.js'],
    // entry: './admin/admin.js',
    output: {
        filename: 'admin.js',
        publicPath: '/',
        path: path.resolve(__dirname, 'dist')
    },
    // ---
    devtool: 'inline-cheap-source-map',
    stats: {
        colors: true,
        reasons: true
    },
    // ---
    module: {
        rules : [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/preset-react'
                        ],
                        plugins: [
                            "@babel/plugin-proposal-class-properties",
                            "@babel/plugin-syntax-dynamic-import",
                            // "@babel/plugin-transform-runtime"
                        ]
                    }
                }
    
            }
        ]
        // noParse: ['./admin/admin.js']
    },
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },
    devServer: {
        host: 'localhost',
        // index: '/index.html',
        // publicPath: '/dist/',
        // // contentBase: __dirname + '/static/crudl-admin-rest',
        contentBase: './dist',
        port:9090,
        hot:true,
        clientLogLevel: 'warning',
        historyApiFallback: true
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: 'index.html',
            // filename: 'index.html'
        })
    ]
}